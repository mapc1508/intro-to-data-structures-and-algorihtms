﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionSolver
{
    class Program
    {
        static void Main(string[] args)
        {
            //var expression = "5+10*12-(12+4)/2";
            //var expression = "((1+2)-(3*4)*(-1))/(3-3)";

            Console.Write("Enter the mathematical expression or q to exit: ");
            var expression = Console.ReadLine();

            while (expression != "q" && !String.IsNullOrEmpty(expression))
            {
                ExpressionSolver.Solve(expression);
                Console.Write("Enter the mathematical expression or q to exit: ");
                expression = Console.ReadLine();
            }
        }
    }

}
