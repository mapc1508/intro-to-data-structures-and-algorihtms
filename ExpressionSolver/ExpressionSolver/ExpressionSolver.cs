using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionSolver
{
    public static class ExpressionSolver
    {
        private static LinkedList<double> numbers = new LinkedList<double>();
        private static LinkedList<Operator> operators = new LinkedList<Operator>();
        private static int step = 0;

        public static double Compute(double x, double y, char operatorCharacter)
        {
            switch (operatorCharacter)
            {
                case '+': return x + y;
                case '-': return x - y;
                case '*': return x * y;
                case '/':
                    if (y == 0) throw new DivideByZeroException(message: "Error: Division by zero.");
                    else return x / y;
                default: return 0;
            }
        }

        private static bool Check(string expression)
        {
            var operatorCharacters = new char[] { '+', '-', '/', '*', '(', ')' };

            char firstCharacter = expression[0], lastCharacter = expression[expression.Length - 1];
            int numOfOpenParenthesis = 0, numOfCloseParenthesis = 0;
            if (!char.IsDigit(firstCharacter) && firstCharacter != '(' && firstCharacter != '-')
            {
                throw new FormatException(message: "Syntax error: Expression must start with a number, open parenthesis or '-'." +
                    $"(First character: {firstCharacter})");
            }
            else if (!char.IsDigit(lastCharacter) && lastCharacter != ')')
            {
                throw new FormatException(message: "Syntax error: Expression must end with a number or a close parenthesis." +
                    $"(Last character: {lastCharacter})");
            }

            for (int i = 0; i < expression.Length; i++)
            {
                var currentCharacter = expression[i];
                string errorMessage = $"Syntax error: Character '{currentCharacter}' on position {i + 1}. ";

                //if current character in iteration is operator
                if (currentCharacter.ToString().IndexOfAny(operatorCharacters) == 0)
                {
                    bool previousIsDigit = false;
                    char? previousCharacter = null;
                    try
                    {
                        previousIsDigit = char.IsDigit(expression[i - 1]);
                        previousCharacter = expression[i - 1];
                    }
                    catch (IndexOutOfRangeException) { }

                    if (currentCharacter == '(')
                    {
                        if (previousCharacter != currentCharacter && previousIsDigit)
                            throw new FormatException(errorMessage + "This character can come only after itself or an arithmetic operator.");
                        //if parenthesis contains only a number
                        if (expression[i + 1] == '-' || char.IsDigit(expression[i + 1]))
                        {
                            int temp = i + 1;
                            if (expression[temp] == '-') temp++;

                            if (step == 1) while (char.IsDigit(expression[temp + 1])) { temp++; }
                            else while (char.IsDigit(expression[temp + 1]) || expression[temp + 1] == ',') { temp++; }
                            //allow double values to occur in expression only in recursive calls
                            if (expression[++temp] == ')')
                            {
                                numbers.AddLast(double.Parse(expression.Substring(i + 1, temp - i - 1)));
                                i = temp;
                                continue;
                            }
                        }
                        numOfOpenParenthesis++;
                    }
                    else if (currentCharacter == ')')
                    {
                        numOfCloseParenthesis++;
                        if (previousCharacter != currentCharacter && !previousIsDigit)
                            throw new FormatException(errorMessage + "This character can come only after itself or a number.");
                    }
                    //for all arithmetic characters
                    else if (previousCharacter != ')' && !previousIsDigit)
                    {
                        if (!expression.StartsWith("-"))
                            throw new FormatException(errorMessage + "This character can come only after a number or a close parenthesis.");
                    }
                    //if no execption is thrown and first character is not '-' add the operator to the operators linkedList
                    if (!(currentCharacter == '-' && i == 0))
                        operators.AddLast(new Operator(currentCharacter));
                }
                //if character is digit iterate to get the complete number
                else if (char.IsNumber(currentCharacter))
                {
                    string numberToString = currentCharacter.ToString();
                    //if first character in expression is '-'
                    if (i == 1 && firstCharacter == '-') numberToString = "-" + numberToString;
                    try
                    {
                        if (step != 1)//allow double values to occur in expression only in recursive calls
                        {
                            while (char.IsDigit(currentCharacter = expression[i + 1]) || currentCharacter == ',')
                            {
                                numberToString += currentCharacter.ToString();
                                i++;
                            }
                        }
                        else
                        {
                            while (char.IsDigit(currentCharacter = expression[i + 1]))
                            {
                                numberToString += currentCharacter.ToString();
                                i++;
                            }
                        }
                    }
                    catch (IndexOutOfRangeException) { }
                    numbers.AddLast(double.Parse(numberToString));
                    continue;
                }
                //if current is not number or one of allowed characters
                else
                {
                    throw new FormatException(message: errorMessage + "Only operators and integer numbers are allowed.");
                }
            }
            //check if number of open and closes parenthesis match
            if (numOfOpenParenthesis != numOfCloseParenthesis)
            {
                throw new FormatException(message: "Syntax error: Number of open and close parenthesis must be the same. " +
                                                    $"(open = {numOfOpenParenthesis}, close = {numOfCloseParenthesis})");
            }
            return true;
        }

        public static double Solve(string expression)
        {
            expression = new string(expression.Where(c => !char.IsWhiteSpace(c)).ToArray());
            Console.WriteLine($"{++step}) {expression}");

            //Base case for recursion: check if the expression is reduced to a single number
            double returnValue = 0;
            if (Double.TryParse(expression.Trim(new char[] { '(', ')' }), out returnValue))
            {
                Console.WriteLine($"The final result is: {expression.Trim(new char[] { '(', ')' })}");
                step = 0;
                return returnValue;
            }

            try
            {
                Check(expression);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                return 0;
            }

            string output = "";
            double result = 0;
            var current = operators.First;
            var next = current.Next;
            var previous = current.Previous;

            while (current != null)
            {
                var previousOperatorPriority = previous == null ? Priority.Undefined : previous.Value.Priority;
                char previousOperatorSymbol = previous == null ? ' ' : previous.Value.Symbol;
                var nextOperatorPriority = next == null ? Priority.Undefined : next.Value.Priority;
                var nextOperatorSymbol = next == null ? ' ' : next.Value.Symbol;

                //if current symbol in iteration is parenthesis
                if (current.Value.Priority == Priority.High)
                {
                    output += current.Value.Symbol;
                }
                else
                {
                    //Compute method will be invoked if this criteria is met
                    if ((current.Value.Priority >= previousOperatorPriority || previousOperatorSymbol == '(') &&
                        (nextOperatorPriority <= current.Value.Priority || nextOperatorSymbol == ')'))
                    {
                        var firstNumberNode = numbers.First;
                        double firstNumberValue = firstNumberNode.Value;
                        double secondNumberValue = firstNumberNode.Next.Value;

                        while (current.Value.Priority >= nextOperatorPriority || nextOperatorSymbol == ')' || current == operators.Last)
                        {
                            try
                            {
                                result = Compute(firstNumberValue, secondNumberValue, current.Value.Symbol);
                            }
                            catch (DivideByZeroException ex)
                            {
                                Console.WriteLine(ex.Message);
                                step = 0;
                                numbers = new LinkedList<double>();
                                operators = new LinkedList<Operator>();
                                return 0;
                            }

                            firstNumberValue = result;//to comute multiple independent steps
                            firstNumberNode = firstNumberNode.Next;
                            numbers.RemoveFirst();

                            if (firstNumberNode != numbers.Last)
                                secondNumberValue = firstNumberNode.Next.Value;

                            if (nextOperatorSymbol == ')' || current.Value.Priority > nextOperatorPriority) break;

                            //move to next
                            if (current != operators.Last)
                            {
                                current = next;
                                next = next.Next;
                                nextOperatorPriority = next == null ? Priority.Undefined : next.Value.Priority;
                                nextOperatorSymbol = next == null ? ' ' : next.Value.Symbol;
                            }
                            else break;
                        }
                        numbers.RemoveFirst();
                        string resultString = "";

                        //if result has decimal points print only two
                        resultString = result % 1 != 0 ? result.ToString("0.00") : result.ToString();
                        //if result is a negative number put parenthesis around it
                        resultString = result < 0 ? "(" + resultString + ")" : resultString;

                        if (nextOperatorSymbol == ')' || next == null || current.Value.Priority > nextOperatorPriority)
                            output += resultString;
                        else output += resultString + current.Value.Symbol;
                    }
                    //if Compute() was not invoked add characters to output based on the criteria below
                    else
                    {
                        if (previousOperatorPriority > current.Value.Priority && previousOperatorSymbol != '(')
                        {
                            if (nextOperatorPriority >= current.Value.Priority && nextOperatorSymbol != ')')
                                output += current.Value.Symbol;
                            else
                            {
                                string numberToString = numbers.First.Value < 0 ? "(" + numbers.First.Value.ToString() + ")"
                                    : numbers.First.Value.ToString();

                                output += current.Value.Symbol + numberToString;
                                numbers.RemoveFirst();
                            }
                        }
                        else
                        {
                            output += numbers.First.Value.ToString() + current.Value.Symbol;
                            numbers.RemoveFirst();
                        }
                    }
                }
                previous = current;
                current = next;
                next = next == null ? null : current.Next;
            }
            operators = new LinkedList<Operator>();//clear the operators linkedList
            return Solve(output);//call 
        }
    }

}