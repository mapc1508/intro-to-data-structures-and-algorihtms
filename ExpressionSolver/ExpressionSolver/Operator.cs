﻿using System.IO;

namespace ExpressionSolver
{
    public enum Priority
    {
        Undefined, Low, Medium, High
    }

    public class Operator
    {
        public Operator(char symbol)
        {
            Symbol = symbol;
            switch (symbol)
            {
                case '+':
                case '-':
                    Priority = Priority.Low;
                    break;
                case '*':
                case '/':
                    Priority = Priority.Medium;
                    break;
                case '(':
                case ')':
                    Priority = Priority.High;
                    break;
            }
        }
        public char Symbol { get; set; }
        public Priority Priority { get; }
    }
}