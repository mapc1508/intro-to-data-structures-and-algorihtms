﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            var chessBoard = new int[8, 8];
            var knight = new Knight(3, 3);
            Console.WriteLine("Initial position " + knight.PrintPosition() + "\n");

            for (int i = -2; i < 3; i++)
            {
                for (int y = -2; y < 3; y++)
                {
                    if (knight.Move(i, y) != "") Console.WriteLine(knight.PrintPosition());
                    knight = new Knight(3, 3);
                }
            }
        }

        public class Knight
        {
            public Knight(int row, int col)
            {
                Row = row;
                Column = col;
            }

            public int Row { get; set; } = 0;
            public int Column { get; set; } = 0;

            public string Move(int moveRow, int moveCol)
            {
                if (CheckMove(moveRow, moveCol))
                {
                    Row = Row + moveRow;
                    Column = Column + moveCol;
                    return PrintPosition();
                }
                return "";
            }

            private bool CheckMove(int moveRow, int moveCol)
            {
                var sum = moveRow + moveCol;
                var outOfTheBoundsRow = moveRow + this.Row > 7 || moveRow + this.Row < 0;
                var outOfTheBoundsCol = moveCol + this.Column > 7 || moveCol + this.Column < 0;

                if (moveRow == 0 || moveCol == 0) return false;
                else if ((moveRow > 2 || moveCol > 2) || (moveRow < -2 || moveCol < -2)) return false;
                else if (sum != 3 && sum != -1 && sum != 1 && sum != -3) return false;
                else if (outOfTheBoundsCol || outOfTheBoundsRow) return false;
                else return true;
            }

            public string PrintPosition()
            {
                var rowName = "";
                var colName = (Column + 1).ToString();

                switch (Row)
                {
                    case 0:
                        rowName = "A";
                        break;
                    case 1:
                        rowName = "B";
                        break;
                    case 2:
                        rowName = "C";
                        break;
                    case 3:
                        rowName = "D";
                        break;
                    case 4:
                        rowName = "E";
                        break;
                    case 5:
                        rowName = "F";
                        break;
                    case 6:
                        rowName = "G";
                        break;
                    case 7:
                        rowName = "H";
                        break;
                }

                return rowName + colName;
            }
        }
    }
}
