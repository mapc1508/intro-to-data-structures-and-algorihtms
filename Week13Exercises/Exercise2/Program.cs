﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//A zero-indexed array A consisting of N different integers is given.The
//array contains integers in the range[1..(N + 1)], which means that
//exactly one element is missing.
//Your goal is to find that missing element.
//Write a function int solution(int A[], int N); that, given a zero-indexed
//array A, returns the value of the missing element.
//Expected worst case time complexity is O(N).

namespace Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            var array = new int[] { 4, 2, 6, 1, 3 };
            Console.WriteLine(FindMissingNumber(array));
        }

        static int FindMissingNumber(int[] A)
        {
            var sumWithoutMissing = 0;
            var sumMissing = 0;

            for (int i = 1; i < A.Length + 2; i++)
            {
                sumWithoutMissing += i;
            }

            foreach (var num in A)
            {
                sumMissing += num;
            }

            return sumWithoutMissing - sumMissing;
        }
    }
}
