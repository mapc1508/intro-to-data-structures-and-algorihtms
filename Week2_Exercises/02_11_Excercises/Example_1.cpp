// 02_11_Excercises.cpp : Defines the entry point for the console application.
//

/*
� For input string determine if it the same whether you read it from left
of from right side. As a result, print �Yes� or �No�. Make sure that the
input string does not contain whitespaces. The program should be
case sensitive.
For example:
- For input �programming� the result is �No�.
- For input �abba� the result is �Yes�.
- For input �Abba� the result is �No�.
*/

#include "stdafx.h"
#include <iostream>
#include <sstream>
using namespace std;

int main()
{
	string word = "";
	cout << "Enter the word you want to check: ";
	cin >> word;

	int y = word.length() - 1;

	for (int i = 0; i < word.length(); i++)
	{
		if (word[i] != word[y]) {
			cout << "No\n";
			break;
		}
		else if (i >= y) {
			cout << "Yes\n";
			break;
		}

		y--;
	}	
	return 0;
}

