// Example_2.cpp : Defines the entry point for the console application.
//

/*
� For input sentence, output the same sentence in upper case. If any
character is already in upper case or not a letter, leave it as such.
For example:
- Input: �This is example 6.�; Output: �THIS IS EXAMPLE 6.�
*/

#include "stdafx.h"
#include <iostream>
#include <sstream>
using namespace std;

int main()
{
	string s;
	cout << "Enter the sentence: ";
	getline(cin, s);

	for (int i = 0; i < s.length(); i++)
	{
		//char c = toupper(s[i]);
		char letter = s[i];
		if ((int)letter >= 97 && (int)letter <= 122)  {
			int c = letter - 32;
			cout << (char)c;
		}
		else {
			cout << letter;
		}
	}
	
	cout << endl;
	return 0;
}

