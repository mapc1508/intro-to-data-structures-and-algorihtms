// Example3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

/*
	For an input sentence, output it with words in reverse order. You may
	assume that any set of characters between two whitespaces is a
	word. If there are two whitespaces next to one another, consider
	them as one whitespace and output as a single blank space.
	For example:
		- Input: �This is exercise 7.�; �7. exercise is This�
		- Input: �Hello�; Output: �Hello�
		- Input: �Hello World�; Output: �World Hello�
*/

int main()
{
	string sentence;
	cout << "Enter the sentence: ";
	getline(cin, sentence);

	int numberOfSpaces = 0;
	int wordLength = 0;

	for (int i = sentence.length()-1; i >= 0; i--)
	{
		if (sentence[i] == ' ') {
			cout << sentence.substr(i + 1, wordLength) << " ";
			while (sentence[i-1] == ' ') i--;
			wordLength = 0;
		}
		else {
			wordLength++;
			if (i == 0)
			{
				cout << sentence.substr(i, wordLength) << endl;
			}
		}		
	}
}

