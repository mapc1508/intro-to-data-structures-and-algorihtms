// Example_4.cpp : Defines the entry point for the console application.
//

/*
� For an input sentence, output it with every individual word in reverse
order. You may assume that any set of characters between two
whitespaces is a word. If there are two whitespaces next to one
another, consider them as one whitespace and output as a single
blank space.
For example:
- Input: �This is exercise 7.�; �sihT si esicrexe .7�
- Input: �Hello�; Output: �olleH�
- Input: �Hello World�; Output: �olleH dlroW�
*/

#include "stdafx.h"
#include "iostream"
#include "sstream"
#include "string"

using namespace std;

void printBackwards(int wordLength, int x, string word) {
	for (int y = 0; y < wordLength; y++)
	{
		x--;
		cout << word[x];
	}
}

int main()
{
	string sentence;
	cout << "Enter the sentence: ";
	getline(cin, sentence);

	int wordLength = 0;

	/*for (int i = 0; i < sentence.length(); i++)
	{
		int x = i;
		if (sentence[i] == ' ') {
			printBackwards(wordLength, x, sentence);
			cout << " ";
			while (sentence[i + 1] == ' ') i++;
			wordLength = 0;
		}
		else {
			wordLength++;
			if (i == sentence.length() - 1) {
				x++;
				printBackwards(wordLength, x, sentence);
				cout << endl;
			}
		}
	}*/

	for (int i = 0; i < sentence.length(); i++)
	{
		
	}
	return 0;
}