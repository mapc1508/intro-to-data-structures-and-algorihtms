﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise5
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<int>();

            for (int i = 0; i < 100; i++)
            {
                numbers.Add(i);
            }

            MergeSort(numbers);

            foreach (var num in numbers)
            {
                Console.WriteLine(num);
            }
        }


        public static void MergeSort(List<int> numbers)
        {
            MergeSort(numbers, 0, numbers.Count() - 1);
        }

        private static void MergeSort(List<int> numbers, int lowIndex, int highIndex)
        {
            if (lowIndex < highIndex)
            {
                int middleIndex = (lowIndex + highIndex) / 2;//divide

                var task1 = new Task(() =>
                {
                    MergeSort(numbers, lowIndex, middleIndex);//conquer
                });

                var task2 = new Task(() =>
                {
                    MergeSort(numbers, middleIndex + 1, highIndex);//conquer
                });

                task1.Start();
                task2.Start();
                Task.WaitAll(task1, task2);

                //merge
                var lowerPart = new List<int>();
                var higherPart = new List<int>();


                task1 = new Task(() =>
                {
                    for (int n = lowIndex; n <= middleIndex; n++)
                    {
                        lowerPart.Add(numbers[n]);
                    }
                });

                task2 = new Task(() =>
                {
                    for (int n = middleIndex + 1; n <= highIndex; n++)
                    {
                        higherPart.Add(numbers[n]);
                    }
                });

                task1.Start();
                task2.Start();
                Task.WaitAll(task1, task2);

                int i = 0;//lowerPart increment
                int j = 0;//higherPart increment
                int k = lowIndex;//array increment

                while (i < lowerPart.Count && j < higherPart.Count)
                {
                    if (lowerPart[i] < higherPart[j])
                    {
                        numbers[k] = lowerPart[i];
                        i++;
                    }
                    else
                    {
                        numbers[k] = higherPart[j];
                        j++;
                    }
                    k++;
                }

                while (i < lowerPart.Count)
                {
                    numbers[k] = lowerPart[i];
                    i++;
                    k++;
                }

                while (j < higherPart.Count)
                {
                    numbers[k] = higherPart[j];
                    j++;
                    k++;
                }
            }
        }
    }
}