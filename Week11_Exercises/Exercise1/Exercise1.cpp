// Exercise1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"

using namespace std;

void FillTreeArray(int height, int biggestBranch, char** treeArray, int numOfTrees);
void printTree(char** board, int height, int numOfColumns);

int main()
{
	int numOfTress = 0, height = 0;
	cout << "Enter the number of trees and height: ";
	cin >> numOfTress >> height;
	cout << endl;
	int biggestBranch = (height - 1) * 2 - 1;

	char **treeArray;
	treeArray = new char*[height];
	for (int i = 0; i < height; i++)
	{
		treeArray[i] = new char[biggestBranch*numOfTress];
	}

	FillTreeArray(height, biggestBranch, treeArray, numOfTress);

	for (int i = 0; i < numOfTress; i++)
	{
		printTree(treeArray, height, biggestBranch*numOfTress);
	}
	
    return 0;
}

void FillTreeArray(int height, int biggestBranch, char** treeArray, int numOfTrees) {

	for (int i = 1; i <= height; i++)
	{
		int current = 0;
		int currentArrayItem = 0;
		while (current < numOfTrees) {
			int numOfLeaves = (i * 2) - 1;
			if (i == height) numOfLeaves = 1;
			int numOfSpaces = biggestBranch - numOfLeaves;

			for (int y = 0; y < numOfSpaces / 2; y++)
			{
				treeArray[i - 1][currentArrayItem] = ' ';
				currentArrayItem++;
			}
			for (int y = 0; y < numOfLeaves; y++)
			{
				treeArray[i - 1][currentArrayItem] = '*';
				currentArrayItem++;
			}
			for (int y = 0; y < numOfSpaces / 2; y++)
			{
				treeArray[i - 1][currentArrayItem] = ' ';
				currentArrayItem++;
			}
			current++;
		}
	}
}

void printTree(char** board, int height, int numOfColumns) {
	for (int i = 0; i < height; i++)
	{
		for (int y = 0; y < numOfColumns; y++)
		{
			cout << board[i][y];
		}
		cout << endl;
	}
	cout << endl;
}





