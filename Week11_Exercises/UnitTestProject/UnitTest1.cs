﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string s = "Krule Krule";
            Exercise3.Program.RemoveChar(ref s, 'K');
            Assert.AreEqual(s, "rule rule");
        }
    }
}
