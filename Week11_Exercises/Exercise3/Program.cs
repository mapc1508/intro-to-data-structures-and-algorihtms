﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise3
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the input string: ");
            string input = Console.ReadLine();

            Console.Write("Enter the character to remove: ");
            char charToBeRemoved = Convert.ToChar(Console.ReadLine());

            RemoveChar(ref input, input.Length - 1, charToBeRemoved);
            Console.WriteLine(input);
        }

        private static void RemoveChar(ref string input, int lastCharIndex, char charToRemove)
        {
            if (lastCharIndex < 0) return;

            if (input[lastCharIndex] == charToRemove)
            {
                input = input.Substring(0, lastCharIndex) + input.Substring(lastCharIndex + 1);
            }

            RemoveChar(ref input, lastCharIndex - 1, charToRemove);
        }

        public static void RemoveChar(ref string input, char charToRemove)
        {
            RemoveChar(ref input, input.Length - 1, charToRemove);
        }
    }
}
