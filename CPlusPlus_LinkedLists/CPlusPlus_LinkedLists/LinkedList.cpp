#include "stdafx.h"
#include "iostream"
#include "LinkedList.h"
#include "sstream"

using namespace std;

LinkedList::LinkedList()
{
	length = 0;
	start = NULL;
}

void LinkedList::MakeEmpty()
{
	Node* temp;
	while (start != NULL) {
		temp = start;
		start = start->next;
		delete temp;
	}
	length = 0;
}

bool LinkedList::IsFull() const
{
	Node* newNode;
	try
	{
		newNode = new Node();
		delete newNode;
		return false;
	}
	catch (bad_alloc exception)
	{
		return true;
	}
}

int LinkedList::GetLength()
{
	return length;
}

void LinkedList::InsertItem(Student item)
{
	Node* temp = new Node();
	temp->item = item;

	//Backward building
	/*temp->item = item;
	temp->next = start;
	start = temp;*/

	//Forward building
	if (start == NULL) {
		start = temp;
		end = temp;
	}
	else {
		end->next = temp;
		end = temp;
	}

	length++;
}

void LinkedList::DeleteItem(Student item)
{
	Node* temp = start;
	if (start != NULL)
	{
		if (start->item.GetName() == item.GetName()) {
			start = start->next;
			delete temp;
			length--;
		}
		else {
			/*while (temp->next != NULL && temp->next->item != item) {
				temp = temp->next;
			}*/
			while (temp->next != NULL) {
				if (temp->next->item.GetName() == item.GetName())
				{
					Node* target = temp->next;
					temp->next = target->next;
					delete target;
					length--;
				}
				else {
					temp = temp->next;
				}
			}
		}
	}
}

void LinkedList::Show()
{
	Node* temp = start;
	while (temp != NULL) {
		cout << temp->item.GetName() << endl;
		temp = temp->next;
	}
}

void LinkedList::SwitchItems(Student itemOne, Student itemTwo)
{
	Node* temp = start;

	Node* targetOne = NULL;
	Node* targetTwo = NULL;

	while (temp = temp->next)
	{
		if (temp->item.GetName() == itemOne.GetName())
			targetOne = temp;
		else if (temp->item.GetName() == itemTwo.GetName())
			targetTwo = temp;
	}

	if (targetOne && targetTwo) {
		Student x = Student("");
		x = targetOne->item;
		targetOne->item = targetTwo->item;
		targetTwo->item = x;
	}
	else {
		cout << "Both items must be part of the linked list.\n";
	}
}

LinkedList::~LinkedList()
{
}
