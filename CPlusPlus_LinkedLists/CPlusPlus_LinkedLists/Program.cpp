// CPlusPlus_LinkedLists.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "LinkedList.h"
#include "sstream"

using namespace std;

int main()
{
	LinkedList linkedList;
	const string brakeLoop = "q";
	
	string messagePrompt = "Enter the student's name you want to add to the list: ";
	string name;
	
	cout << messagePrompt;
	cin >> name;
	
	while (name != brakeLoop) {
		linkedList.InsertItem(Student(name));
		cout << messagePrompt;
		cin >> name;
	}

	linkedList.Show();
	
	/*messagePrompt = "Enter the item you want to delete: ";
	cout << messagePrompt;
	cin >> name;
	
	linkedList.DeleteItem(name);*/
	/*cout << endl << linkedList.GetLength();*/


	string name1, name2;
	messagePrompt = "Enter the names you want to switch: ";
	cout << messagePrompt;
	cin >> name1 >> name2;

	linkedList.SwitchItems(Student(name1), Student(name2));

	linkedList.Show();
	
	return 0;
}




