﻿#include "iostream"

class Student
{
public:
	Student(std::string studentName);
	std::string GetName();

private:
	std::string name;
};