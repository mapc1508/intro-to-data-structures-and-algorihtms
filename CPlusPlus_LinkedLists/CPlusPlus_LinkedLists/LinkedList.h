#pragma once

#include "Node.h"

class LinkedList
{
public:
	LinkedList();
	void MakeEmpty();
	bool IsFull() const;
	int GetLength();
	void InsertItem(Student item);
	void DeleteItem(Student item);
	void Show();
	void SwitchItems(Student itemOne, Student itemTwo);
	~LinkedList();
private:
	int length;
	Node* start;
	Node* end;
};