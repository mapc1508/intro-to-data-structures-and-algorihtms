﻿using System;

namespace Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            string result = "";
            ConvertToHexa(2, ref result);
            Console.WriteLine(result);
        }

        static void ConvertToHexa(int decimalNumber, ref string result)
        {
            if (decimalNumber < 1) return;
            result = (decimalNumber % 2).ToString() + result;
            ConvertToHexa(decimalNumber / 2, ref result);
        }
    }
}
