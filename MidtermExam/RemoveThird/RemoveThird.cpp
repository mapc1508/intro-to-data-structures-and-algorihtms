// RemoveThird.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "sstream"

using namespace std;


string RemoveThird(string s);

int main()
{
	cout << RemoveThird("HexHexHexHex") << endl; //mirzamirsoamina
    return 0;
}

string RemoveThird(string s) {
	
	string returnString;
	int previousIndex = 0;

	for (int i = 0; i < s.length(); i += 3)
	{
		if (i == 0) {
			i += 2;
			returnString += s.substr(0, i);
		}
		else
			returnString += s.substr(previousIndex + 1, i - previousIndex - 1);
		previousIndex = i;
	}

	return returnString;
}

