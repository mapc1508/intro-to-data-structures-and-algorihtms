// Exercise3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

class Number
{
public:
	Number(int numberValue);
	int number;
	Number* previous;
	Number* next;
};

Number::Number(int numberValue)
{
	number = numberValue;
	previous = NULL;
	next = NULL;
}

class DoublyLinkedList
{
public:
	DoublyLinkedList();
	int GetLength();
	void InsertItem(int value);
private:
	int length;
	Number* start;
	Number* end;
};

DoublyLinkedList::DoublyLinkedList()
{
	length = 0;
	start = NULL;
	end = NULL;
}

int DoublyLinkedList::GetLength()
{
	return length;
}

void DoublyLinkedList::InsertItem(int value)//2-3-4-5-6
{
	Number* newNumber = new Number(value);
	length++;

	if (start == NULL) {
		start = newNumber;
		end = newNumber;
	}
	else {
		if (length == 2)
			end->previous = start;

		newNumber->previous = end;
		end->next = newNumber;
		end = newNumber;
	}

	start->previous = NULL;
}


int main()
{
	DoublyLinkedList list;
	list.InsertItem(2);
	list.InsertItem(3);
	list.InsertItem(4);
	list.InsertItem(5);
	list.InsertItem(6);
    return 0;
}

