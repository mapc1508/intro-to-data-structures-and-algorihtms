// 28_11_2016_SecondTestPreparation.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "Headers.h"
#include "sstream"

using namespace std;

int main()
{
	/*Student s;
	s.Grades = new Grade[3];

	s.Name = "Mirza";
	
	for (int i = 0; i < 3; i++)
	{
		cout << "Enter subject and its grade: ";
		cin >> s.Grades[i].subject >> s.Grades[i].value;
	}

	cout << s.Name << " grades: " << endl;
	for (int i = 0; i < 3; i++)
	{
		cout << s.Grades[i].subject << " (" << s.Grades[i].value << ")" << endl;
	}*/

	Stack stackOfIntegers;
	stackOfIntegers.Push(5);
	stackOfIntegers.Push(6);
	stackOfIntegers.Push(7);
	stackOfIntegers.Push(8);
	stackOfIntegers.Push(9);

	for (int i = 0; i < stackOfIntegers.length; i++)
	{
		cout << stackOfIntegers.Pop();
	}

	cout << endl;

	return 0;
}

