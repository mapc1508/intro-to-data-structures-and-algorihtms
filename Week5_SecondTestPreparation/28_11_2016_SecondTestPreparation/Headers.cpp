#include "stdafx.h"
#include "Headers.h"
#include "iostream"

using namespace std;

Student::Student()
{
	
}

Student::~Student()
{
}

Grade::Grade()
{
}

Grade::~Grade()
{
}

Stack::Stack() {
	length = 0;
	start = NULL;
	end = NULL;
}

int Stack::Push(int value)
{
	Node* temp = new Node();
	temp->item = value;
	if (start == NULL) {
		start = temp;
		end = temp;
	}
	else {
		end->next = temp;
		end = temp;
	}
	length++;
	return value;
}

int Stack::Pop()
{
	Node* temp = start;
	int valueToReturn = 0;
	if (start->next == NULL) {
		valueToReturn = temp->item;
		delete temp;
		return valueToReturn;
	}

	while (temp->next != end) {
		temp = temp->next;
	}
	valueToReturn = end->item;
	end = temp;
	end->next = NULL;
	temp = temp->next;
	delete temp;
	return valueToReturn;
}


Node::Node()
{
}


