// Exercise1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"

using namespace std;

//void DeepCopy(int* array1, int* &array2, int arrayLength);

void ShallowCopy(int* array1, int* &array2);

int main()
{
	int* arrayOne = new int[5];
	int* arrayTwo;
	
	for (int i = 0; i < 5; i++)
	{
		arrayOne[i] = i * 10;
	}

	ShallowCopy(arrayOne, arrayTwo);

	for (int i = 0; i < 5; i++)
	{
		arrayTwo[i] = i * 11;
	}

	for (int i = 0; i < 5; i++)
	{
		cout << arrayOne[i] << endl;
	}

	cout << endl;

	for (int i = 0; i < 5; i++)
	{
		cout << arrayTwo[i] << endl;
	}

	return 0;
}

void DeepCopy(int* array1, int* &array2, int arrayLength)
{
	array2 = new int[arrayLength];
	for (int i = 0; i < arrayLength; i++)
		array2[i] = array1[i];
}

void ShallowCopy(int* array1, int* &array2)
{
	array2 = array1;
}
