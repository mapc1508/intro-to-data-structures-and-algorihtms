﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week9_Threading
{
    class Program
    {
        static void Main(string[] args)
        {
            var randomGenerator = new Random();
            var numbers = new List<int>();
            numbers.Add(5000);

            for (int i = 0; i < 1000; i++)
            {
                numbers.Add(randomGenerator.Next(1, 10000));
            }

            numbers = numbers.OrderBy(n => n).Distinct().ToList();

            var stopwatch = new Stopwatch();

            stopwatch.Start();
            PrintBinaryAsync(numbers);
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed);

            stopwatch.Reset();

            stopwatch.Start();
            PrintBinary(numbers);
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed);
        }

        static async void PrintBinaryAsync(IEnumerable<int> numbers)
        {
            var t1 = await Task.FromResult(Search.BinarySearch(5000, numbers));
        }

        static void PrintBinary(IEnumerable<int> numbers)
        {
            int t1 = Search.BinarySearch(5000, numbers);
        }
    }

    public static class Search
    {
        private static void BinarySearch(int number, IEnumerable<int> numbers, int minIndex, int maxIndex, ref int result)//1,2,3,4,5,6,7 => 
        {
            if (maxIndex < minIndex)
            {
                result = -1;
                return;
            }

            int midIndex = (minIndex + maxIndex + 1) / 2;
            var middleNumber = numbers.ToArray()[midIndex];

            if (middleNumber == number)
            {
                result = midIndex;
                return;
            }
            else if (number > middleNumber) minIndex = midIndex + 1;
            else maxIndex = midIndex - 1;

            BinarySearch(number, numbers, minIndex, maxIndex, ref result);
        }

        public static int BinarySearch(int number, IEnumerable<int> numbers)
        {
            int result = -1;
            BinarySearch(number, numbers, 0, numbers.Count() - 1, ref result);
            return result;
        }
    }

}
