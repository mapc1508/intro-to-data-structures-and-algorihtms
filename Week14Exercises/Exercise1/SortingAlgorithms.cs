﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise1
{
    public class SortingAlgorithms
    {
        public CancellationToken CancelationToken { get; set; }

        public void BubbleSort(List<int> numbers)
        {
            bool changed = true;
            while (changed)
            {
                changed = false;
                for (int i = 0; i < numbers.Count - 1; i++)
                {
                    if (CancelationToken.IsCancellationRequested) return;

                    if (numbers[i] > numbers[i + 1])
                    {
                        int temp = numbers[i];
                        numbers[i] = numbers[i + 1];
                        numbers[i + 1] = temp;
                        changed = true;
                    }
                }
            }
        }


        public void SelectionSort(List<int> numbers)
        {
            int starting = 0;

            while (starting < numbers.Count)
            {
                int smallestIndex = 0;
                int smallest = Int32.MaxValue;

                for (int i = starting; i < numbers.Count; i++)
                {
                    if (CancelationToken.IsCancellationRequested) return;

                    if (numbers[i] < smallest)
                    {
                        smallest = numbers[i];
                        smallestIndex = i;
                    }
                }

                int temp = numbers[starting];
                numbers[starting] = numbers[smallestIndex];
                numbers[smallestIndex] = temp;
                starting++;
            }
        }


        public void MergeSort(List<int> numbers)
        {
            MergeSort(numbers, 0, numbers.Count() - 1);
        }

        private void MergeSort(List<int> numbers, int lowIndex, int highIndex)
        {
            if (lowIndex < highIndex)
            {
                if (CancelationToken.IsCancellationRequested) return;
                int middleIndex = (lowIndex + highIndex) / 2;//divide
                MergeSort(numbers, lowIndex, middleIndex);//conquer
                MergeSort(numbers, middleIndex + 1, highIndex);//conquer

                //merge
                var lowerPart = new List<int>();
                var higherPart = new List<int>();


                for (int n = lowIndex; n <= middleIndex; n++)
                {
                    lowerPart.Add(numbers[n]);
                }

                for (int n = middleIndex + 1; n <= highIndex; n++)
                {
                    higherPart.Add(numbers[n]);
                }

                int i = 0;//lowerPart increment
                int j = 0;//higherPart increment
                int k = lowIndex;//array increment

                while (i < lowerPart.Count && j < higherPart.Count)
                {
                    if (lowerPart[i] < higherPart[j])
                    {
                        numbers[k] = lowerPart[i];
                        i++;
                    }
                    else
                    {
                        numbers[k] = higherPart[j];
                        j++;
                    }
                    k++;
                }

                while (i < lowerPart.Count)
                {
                    numbers[k] = lowerPart[i];
                    i++;
                    k++;
                }

                while (j < higherPart.Count)
                {
                    numbers[k] = higherPart[j];
                    j++;
                    k++;
                }
            }
        }
    }
}
