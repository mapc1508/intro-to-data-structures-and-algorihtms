﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise1
{
    static class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Starting position " + ConvertToPosition(3, 5));
            //foreach (var position in CheckQueenMoves(3, 5))
            //{
            //    Console.Write(position + " ,");
            //}

            //Console.WriteLine(SumOddMinusSumEven(new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }));

            //int x = ConvertStringToInt("2314");

            //SortingInParallel();

            //var numbers = new List<int>() { 2, 3, 1, 0 };

            //SortNumbers(numbers);

            //foreach (var num in numbers)
            //{
            //    Console.WriteLine(num);
            //}

            Djikstra(new List<Node>() { new Node() { Point1 = "A", Point2 = "B", Distance = 2 },
                        new Node() { Point1 = "A", Point2 = "E", Distance = 4 },
                        new Node() { Point1 = "B", Point2 = "C", Distance = 3 },
                        new Node() { Point1 = "B", Point2 = "E", Distance = 3 },
                        new Node() { Point1 = "C", Point2 = "D", Distance = 4 },
                        new Node() { Point1 = "E", Point2 = "D", Distance = 5 } });
        }

        private static void SortingInParallel()
        {
            var numbers = new List<int>();
            var randomGenerator = new Random();
            for (int i = 0; i < 10000; i++)
            {
                numbers.Add(randomGenerator.Next(0, 10000));
            }

            var sortingAlgorithms = new SortingAlgorithms();

            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            sortingAlgorithms.CancelationToken = token;

            var tasks = new List<Task>();

            tasks.Add(Task.Run(() =>
            {
                sortingAlgorithms.SelectionSort(numbers);
            }));

            tasks.Add(Task.Run(() =>
            {
                sortingAlgorithms.BubbleSort(numbers);
            }));

            tasks.Add(Task.Run(() =>
            {
                sortingAlgorithms.MergeSort(numbers);
            }));

            int completedId = Task.WaitAny(tasks.ToArray());
            Console.WriteLine("Task that has completed first is " + completedId);
            tokenSource.Cancel();

            foreach (var number in numbers)
            {
                Console.WriteLine(number);
            }
        }

        static IEnumerable<string> CheckQueenMoves(int row, int col)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (!(j == col || i == row || (i + j == row + col) || (j - i == col - row) || (i == row && j == col)))
                        yield return ConvertToPosition(i, j);
                }
            }
        }

        static string ConvertToPosition(int row, int col)
        {
            string result = "";
            switch (row)
            {
                case 0:
                    result += "A";
                    break;
                case 1:
                    result += "B";
                    break;
                case 2:
                    result += "C";
                    break;
                case 3:
                    result += "D";
                    break;
                case 4:
                    result += "E";
                    break;
                case 5:
                    result += "F";
                    break;
                case 6:
                    result += "G";
                    break;
                case 7:
                    result += "H";
                    break;
                default: return "";
            }

            if (col > 7 || row < 0) return "";
            result += (col + 1).ToString();
            return result;
        }

        static int SumOddMinusSumEven(List<int> integers)
        {
            var sumEven = 0;
            var sumOdd = 0;

            for (int i = 0; i < integers.Count; i++)
            {
                if (i % 2 == 0) sumEven += integers[i];
                else sumOdd += integers[i];
            }

            return sumOdd - sumEven;
        }

        static int SumOddMinusSumEvenParallel(List<int> integers)
        {
            var sumEven = 0;
            var sumOdd = 0;

            Parallel.For(0, integers.Count, i =>
            {
                if (i % 2 == 0) sumEven += integers[i];
                else sumOdd += integers[i];
            });

            return sumOdd - sumEven;
        }

        static int ConvertStringToInt(string input)
        {
            var decimalValue = (int)Math.Pow(10, input.Length - 1);
            var result = 0;

            foreach (var item in input)
            {
                var x = item - 48;
                result += decimalValue * (item - 48);
                decimalValue /= 10;
            }

            return result;
        }

        public static void SortNumbers<T>(List<T> numbers) where T : IComparable<T>
        {
            var type = typeof(T);
            var isSuported = type == typeof(double) || type == typeof(int) || type == typeof(long);
            if (!isSuported) throw new InvalidOperationException();

            bool changed = true;
            while (changed)
            {
                changed = false;
                for (int i = 0; i < numbers.Count - 1; i++)
                {
                    if (numbers[i].CompareTo(numbers[i + 1]) > 0)
                    {
                        var temp = numbers[i];
                        numbers[i] = numbers[i + 1];
                        numbers[i + 1] = temp;
                        changed = true;
                    }
                }
            }
        }

        static int FindMissingNumber(int[] A)
        {
            var sumWithoutMissing = 0;
            var sumMissing = 0;

            for (int i = 1; i < A.Length + 2; i++)
            {
                sumWithoutMissing += i;
            }

            foreach (var num in A)
            {
                sumMissing += num;
            }

            return sumWithoutMissing - sumMissing;
        }

        public static void Djikstra(List<Node> nodes)
        {
            var vertices = new Dictionary<string, int>();

            foreach (var node in nodes)
            {
                if (node == nodes.First()) vertices.Add(node.Point1, 0);
                else
                {
                    if (!vertices.ContainsKey(node.Point1)) vertices.Add(node.Point1, int.MaxValue);
                    if (!vertices.ContainsKey(node.Point2)) vertices.Add(node.Point2, int.MaxValue);
                }
            }

            var current = vertices.OrderBy(v => v.Value).First();
            KeyValuePair<string, int> neighbor;

            while (vertices.Last().Value == int.MaxValue)
            {
                foreach (var item in nodes.Where(v => v.Point1 == current.Key))
                {
                    neighbor = vertices.First(v => v.Key == item.Point2);
                    if (neighbor.Value > item.Distance + current.Value) neighbor = new KeyValuePair<string, int>(neighbor.Key, item.Distance + current.Value);
                }

                vertices.Remove(current.Key);
                current = vertices.OrderBy(v => v.Value).First();
            }
        }
    }
}

