﻿namespace Exercise1
{
    public class Node
    {
        public string Point1 { get; set; }
        public string Point2 { get; set; }
        public int Distance { get; set; }
    }
}