﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week11_Quiz3
{
    class Program
    {
        static void Main(string[] args)
        {
            var characters = "Mirza".ToList();

            BubbleSort(characters);

            foreach (var character in characters)
            {
                Console.WriteLine(character);
            }


            //var numbers = new List<int>() { 1, 2, 3, 4, 5, 0, 8, 9, 0, 6, 0 };
            var numbers = new List<int>();
            var randomGenerator = new Random();

            for (int i = 0; i < 1000000; i++)
            {
                numbers.Add(randomGenerator.Next(0, 10));
            }

            var countOfZeros = numbers.Where(n => n == 0).ToList();
            Console.WriteLine(countOfZeros.Count);

            Console.WriteLine("Number of zeros in a list is: " + CountZerosParallelForeach(numbers));
            Console.WriteLine("Number of zeros in a list is: " + CountZerosTasks(numbers));

        }

        public static void BubbleSort(List<char> characters)
        {
            bool changed = true;
            while (changed)
            {
                changed = false;
                for (int i = 0; i < characters.Count - 1; i++)
                {
                    if (characters[i] > characters[i + 1])
                    {
                        var temp = characters[i];
                        characters[i] = characters[i + 1];
                        characters[i + 1] = temp;
                        changed = true;
                    }
                }
            }
        }

        //Not safe for a large list of integers
        public static int CountZerosParallelForeach(List<int> integers)
        {
            int total = 0;

            Parallel.ForEach(integers, num =>
            {
                if (num == 0) total++;
            });

            return total;
        }

        public static int CountZeros(List<int> integers)
        {
            int count = 0;
            foreach (var num in integers)
            {
                if (num == 0)
                {
                    count++;
                }
            }

            return count;
        }

        public static int CountZerosTasks(List<int> integers)
        {
            int count1 = 0, count2 = 0;

            var task1 = new Task(() =>
            {
                count1 = CountZeros(integers.Take(integers.Count / 2).ToList());
            });

            var task2 = new Task(() =>
            {
                count2 = CountZeros(integers.Skip(integers.Count / 2).ToList());
            });

            task1.Start();
            task2.Start();
            Task.WaitAll(task1, task2);

            return count1 + count2;
        }
    }
}
