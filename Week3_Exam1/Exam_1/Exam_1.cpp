// Exam_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "string"

using namespace std;


int main()
{
	int x, y;
	bool validInput;
	do
	{
		validInput = true;
		cout << "Enter x: ";
		cin >> x;
		cout << "Enter y: ";
		cin >> y;

		if (x <= 0 || y <= 0) {
			cout << "Input is not valid, please try again" << endl;
			validInput = false;
		}
	} while (validInput == false);

	//Align horizontally
	for (int i = 0; i < x; i++)
	{
		for (int q = 0; q < x; q++)
		{
			for (int j = 0; j < y; j++)
			{
				if (i == 0 || i == x - 1) {
					cout << "#";
				}
				else {
					if (j == 0 || j == y - 1) {
						cout << "#";
					}
					else cout << " ";
				}
			}
			cout << " ";
		}
		cout << endl;
	}

	//Stack vertically
	/*for (int s = 0; s < y; s++)
	{
		for (int i = 0; i < x; i++)
		{
			for (int q = 0; q < x; q++)
			{
				for (int j = 0; j < y; j++)
				{
					if (i == 0 || i == x - 1) {
						cout << "#";
					}
					else {
						if (j == 0 || j == y - 1) {
							cout << "#";
						}
						else cout << " ";
					}
				}
				cout << " ";
			}
			cout << endl;
		}
	}*/

	return 0;
}
