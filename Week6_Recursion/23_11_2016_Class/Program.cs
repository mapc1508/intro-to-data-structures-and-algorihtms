﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_11_2016_Class
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(factorial(5));

            //Console.WriteLine(fibonacci(7));

            //string result = "";
            //convertToBinary(23, ref result);
            //Console.WriteLine(result);

            //var numbers = new int[] { 3, 5, 7, 8, 9, 1, 2, 9 };
            ////var numbers = new List<int>();
            ////var random = new Random();
            ////for (int i = 0; i < 50; i++)
            ////{
            ////    numbers.Add(random.Next(1, 100));
            ////}
            //Console.WriteLine(binaryTreeSearch(1, numbers));

            string path = @"C:\Users\mapc1\Dropbox\Subjects\Network technologies";
            var allFiles = FindAllFiles(path).OrderBy(f => f);
            foreach (var file in allFiles)
            {
                Console.WriteLine(file);
            }
            Console.WriteLine(allFiles.Count());

        }

        private static IEnumerable<string> FindAllFiles(string path)
        {
            var files = new List<string>();
            if (Directory.GetFiles(path).Count() > 0)
            {
                var filesInRoot = Directory.EnumerateFiles(path);
                foreach (var file in filesInRoot)
                {
                    files.Add(file);
                }
            }
            var directories = Directory.EnumerateDirectories(path);
            foreach (var directory in directories)
            {
                files.AddRange(Directory.EnumerateFiles(directory));
            }
            foreach (var file in files)
            {
                yield return file.Substring(file.LastIndexOf('\\') + 1);
            }
            if (directories.Count() == 0) yield return null;
            else
            {
                foreach (var directory in directories)
                {
                    FindAllFiles(directory);
                }
            }
        }

        private static int fibonacci(int n)
        {
            if (n == 1 || n == 2) return 1;//1,1,2,3,5,8,13
            return fibonacci(n - 1) + fibonacci(n - 2);
        }

        private static int factorial(int n)
        {
            if (n == 0) return 1;
            return factorial(n - 1) * n;
        }

        private static void convertToBinary(int decimalNumber, ref string result)
        {
            result = Convert.ToString(decimalNumber % 2) + result;
            if (decimalNumber == 1) return;
            else convertToBinary(decimalNumber / 2, ref result);
        }

        private static bool binaryTreeSearch(int number, IEnumerable<int> numbers)
        {
            numbers = numbers.OrderBy(n => n);
            if (numbers.Count() == 2)
            {
                if (numbers.First() == number || numbers.Last() == number) return true;
                else return false;
            }
            int middle = numbers.Count() / 2;
            int middleNumber = numbers.ElementAt(middle);
            if (middleNumber == number) return true;
            else if (number > middleNumber) numbers = numbers.Where(n => n >= middleNumber).Distinct();
            else numbers = numbers.Where(n => n <= middleNumber).Distinct();
            return binaryTreeSearch(number, numbers);
        }
    }
}
