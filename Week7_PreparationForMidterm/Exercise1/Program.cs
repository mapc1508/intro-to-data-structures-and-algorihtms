﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    //Calculate x^y using recursion
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Power(2, 2));
        }

        static long Power(long number, int power)
        {
            if (power == 0) return 1;
            return number *= Power(number, power - 1);
        }
    }
}