﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 Given a string of parenthesis, check if parenthesis are in a correct
order (each one closed properly). Correct order examples: „()“,
„()()()“, „(()(()))“. If any other symbol is encountered, end the
program with proper message.
*/

namespace Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            string expression = "(()(()))";
            try
            {
                if (CheckParenthesisRecursion(expression)) Console.WriteLine("Expression is correct.");
                else Console.WriteLine("Parenthesis are not closed properly.");
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static bool CheckParenthesis(string expression)
        {
            for (int i = 0; i < expression.Length; i++)
            {
                if (expression[i] != '(' && expression[i] != ')')
                {
                    throw new FormatException(message: "Only '(' and ')' are permitted.");
                }
                else if (i == expression.Length - 1)
                {
                    return false;
                }
                else if (expression[i] == '(' && expression[i + 1] == ')')
                {
                    expression = expression.Remove(i, 2);
                    i = -1;
                }
            }
            return true;
        }

        static bool CheckParenthesisRecursion(string expression)
        {
            if (expression.Where(c => c != '(' && c != ')').ToArray().Length > 0)
            {
                throw new FormatException(message: "Only '(' and ')' are permitted.");
            }
            if (expression.Length == 0) return true;
            else
            {
                if (expression.Contains("()"))
                {
                    return CheckParenthesisRecursion(expression.Replace("()", ""));
                }
                else return false;
            }
        }
    }
}