﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(BlocksInTriangle(4));
        }

        static int BlocksInTriangle(int rows)
        {
            if (rows <= 1) return 1;

            return BlocksInTriangle(rows - 1) + rows;
        }
    }
}
