﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 Input a sentence. Make sure it only contains english alphabet
characters (a-z, A-Z). Make a camelCase string out of it.
*/

namespace Exercise3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(ConvertToCamel("hOW are YOU"));
        }


        static string ConvertToCamel(string sentence)
        {
            var words = sentence.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string formattedSentence = "";

            foreach (var word in words)
            {
                if (char.IsLetter(word.First()))
                {
                    if (word == words.First()) formattedSentence += word.ToLower();
                    else
                    {
                        formattedSentence += word.First().ToString().ToUpper() + word.Substring(1).ToLower();
                    }
                }
                else
                {
                    formattedSentence += word;
                }
            }
            return formattedSentence;
        }
    }
}