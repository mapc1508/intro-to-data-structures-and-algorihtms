﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Write a recursive program, which generates and prints all permutations
of the numbers 1, 2, …, n, for a given integer n. Example input:
n = 3
Example output:
(1, 2, 3), (1, 3, 2), (2, 1, 3), (2, 3, 1), (3, 1, 2), (3, 2, 1)     
*/

namespace Exercise5
{
    class Program
    {
        static void Main(string[] args)
        {
            permutation("ABCD");
        }

        public static void permutation(String str)
        {
            permutation("", str);
        }

        private static void permutation(String prefix, String str)
        {
            int n = str.Length;
            if (n == 0) Console.WriteLine(prefix);
            else
            {
                for (int i = 0; i < n; i++)
                    permutation(prefix + str[i], str.Substring(0, i) + str.Substring(i + 1));
            }
        }
    }
}
