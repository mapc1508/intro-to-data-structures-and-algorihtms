﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<int>() { 4, 5, 9, 10, 1, 6, 8 };
            //SortingAlgorithms.SelectionSort(numbers);
            SortingAlgorithms.QuickSort(numbers, 0, numbers.Count - 1);

            Print(numbers);
        }

        private static void Print(List<int> numbers)
        {
            foreach (var number in numbers)
            {
                Console.WriteLine(number);
            }
        }
    }

    public class SortingAlgorithms
    {

        public static void BubbleSort(List<int> numbers)
        {
            bool changed = true;
            while (changed)
            {
                changed = false;
                for (int i = 0; i < numbers.Count - 1; i++)
                {
                    if (numbers[i] > numbers[i + 1])
                    {
                        int temp = numbers[i];
                        numbers[i] = numbers[i + 1];
                        numbers[i + 1] = temp;
                        changed = true;
                    }
                }
            }
        }

        public static void SelectionSort(List<int> numbers)
        {
            int starting = 0;

            while (starting < numbers.Count)
            {
                int smallestIndex = 0;
                int smallest = Int32.MaxValue;

                for (int i = starting; i < numbers.Count; i++)
                {
                    if (numbers[i] < smallest)
                    {
                        smallest = numbers[i];
                        smallestIndex = i;
                    }
                }

                int temp = numbers[starting];
                numbers[starting] = numbers[smallestIndex];
                numbers[smallestIndex] = temp;
                starting++;
            }
        }

        public static void InsertionSort(List<int> numbers)
        {
            for (int i = 1; i < numbers.Count; i++)
            {
                Insert(numbers, i - 1, numbers[i]);
            }
        }

        private static void Insert(List<int> numbers, int rightmostIndex, int key)
        {
            int j = 0;

            for (j = rightmostIndex; j >= 0 && numbers[j] > key; j--)
            {
                numbers[j + 1] = numbers[j];
            }

            numbers[j + 1] = key;
        }

        public static void MergeSort(List<int> numbers)
        {
            MergeSort(numbers, 0, numbers.Count() - 1);
        }

        private static void MergeSort(List<int> numbers, int lowIndex, int highIndex)
        {
            if (lowIndex < highIndex)
            {
                int middleIndex = (lowIndex + highIndex) / 2;//divide
                MergeSort(numbers, lowIndex, middleIndex);//conquer
                MergeSort(numbers, middleIndex + 1, highIndex);//conquer

                //merge
                var lowerPart = new List<int>();
                var higherPart = new List<int>();

                for (int n = lowIndex; n <= middleIndex; n++)
                {
                    lowerPart.Add(numbers[n]);
                }

                for (int n = middleIndex + 1; n <= highIndex; n++)
                {
                    higherPart.Add(numbers[n]);
                }

                int i = 0;//lowerPart increment
                int j = 0;//higherPart increment
                int k = lowIndex;//array increment

                while (i < lowerPart.Count && j < higherPart.Count)
                {
                    if (lowerPart[i] < higherPart[j])
                    {
                        numbers[k] = lowerPart[i];
                        i++;
                    }
                    else
                    {
                        numbers[k] = higherPart[j];
                        j++;
                    }
                    k++;
                }

                while (i < lowerPart.Count)
                {
                    numbers[k] = lowerPart[i];
                    i++;
                    k++;
                }

                while (j < higherPart.Count)
                {
                    numbers[k] = higherPart[j];
                    j++;
                    k++;
                }
            }
        }

        public static void QuickSort(List<int> numbers, int lowIndex, int highIndex)
        {
            if (lowIndex < highIndex)
            {
                int pivot = lowIndex;
                int temp = 0;

                for (int j = lowIndex; j < highIndex; j++)
                {
                    if (numbers[j] <= numbers[highIndex])
                    {
                        temp = numbers[j];
                        numbers[j] = numbers[pivot];
                        numbers[pivot] = temp;
                        pivot++;
                    }
                }

                temp = numbers[highIndex];
                numbers[highIndex] = numbers[pivot];
                numbers[pivot] = temp;

                QuickSort(numbers, lowIndex, pivot - 1);
                QuickSort(numbers, pivot + 1, highIndex);
            }
        }
    }
}
