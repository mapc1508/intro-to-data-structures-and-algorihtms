﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


/*
    Count the number of elements in an 
    array/list that satisfy a certain condition (e.g. in a list of integers, count how many of them are less than 10).
 */
namespace Week10_Quiz3Preparation
{
    class Program
    {
        static void Main(string[] args)
        {
            var randomGenerator = new Random();
            var numbers = new List<int>();

            for (int i = 0; i < 100000; i++)
            {
                numbers.Add(i);
            }

            var stopwatch = new Stopwatch();
            //stopwatch.Start();
            //Console.WriteLine("Count: " + CountLessThanTen(numbers));
            //stopwatch.Stop();
            //Console.WriteLine(stopwatch.Elapsed.TotalMilliseconds + " ms");

            //stopwatch.Restart();
            //Console.WriteLine("Count: " + CountLessThanTenParallel(numbers));
            //stopwatch.Stop();
            //Console.WriteLine(stopwatch.Elapsed.TotalMilliseconds + " ms");

            //stopwatch.Restart();
            //int totalCount = CountLessThanTenTask(numbers);
            //stopwatch.Stop();
            //Console.WriteLine("Count: " + (totalCount));
            //Console.WriteLine(stopwatch.Elapsed.TotalMilliseconds + " ms");

            stopwatch.Start();
            Console.WriteLine("Product: " + ListSum.Compute(numbers, 0, numbers.Count));
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.TotalMilliseconds + " ms");

            stopwatch.Restart();
            Console.WriteLine("Product: " + ListSum.ComputeParallel(numbers));
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.TotalMilliseconds + " ms");

            stopwatch.Restart();
            Console.WriteLine("Product: " + ListSum.ComputeUsingTasks(numbers));
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.TotalMilliseconds + " ms");

            stopwatch.Restart();
            Console.WriteLine("Product: " + ListSum.ComputeUsingThreads(numbers));
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.TotalMilliseconds + " ms");
            //// Quickly creating a list integers from 0 to 99999999
            //List<int> list = new List<int>();
            //for (int i = 0; i < 99999999; i++)
            //    list.Add(i);

            //// Measure the time taken for normal (sequential) binary search
            //Console.WriteLine("Without parallelism:");
            //DateTime dt1 = DateTime.Now;
            //Console.WriteLine(binarySearch(123516, list, 0, list.Count - 1));
            //DateTime dt2 = DateTime.Now;
            //Console.WriteLine(dt2.Subtract(dt1).TotalMilliseconds);

            //// Measure the time taken for binary search with using threads
            //Console.WriteLine("With threads:");
            //DateTime dt3 = DateTime.Now;
            //Console.WriteLine(binarySearch_Threads(123516, list));
            //DateTime dt4 = DateTime.Now;
            //Console.WriteLine(dt4.Subtract(dt3).TotalMilliseconds);

            //// Measure the time taken for binary search with using threads
            //Console.WriteLine("With tasks:");
            //DateTime dt5 = DateTime.Now;
            //Console.WriteLine(binarySearch_Tasks(123516, list));
            //DateTime dt6 = DateTime.Now;
            //Console.WriteLine(dt6.Subtract(dt5).TotalMilliseconds);
        }

        private static int CountLessThanTenTask(List<int> numbers)
        {
            //var numbersFirstHalf = new List<int>();
            //var numbersSecondHalf = new List<int>();

            //for (int i = 0; i < numbers.Count; i++)
            //{
            //    if (i < numbers.Count / 2) numbersFirstHalf.Add(numbers[i]);
            //    else numbersSecondHalf.Add(numbers[i]);
            //}

            int count1 = 0, count2 = 0;
            var task1 = new Task(() =>
            {
                count1 = CountLessThanTen(numbers.Take(numbers.Count / 2).ToList());
            });

            var task2 = new Task(() =>
            {
                count2 = CountLessThanTen(numbers.Skip(numbers.Count / 2).ToList());
            });

            task1.Start();
            task2.Start();
            Task.WaitAll(task1, task2);
            int totalCount = count1 + count2;
            return totalCount;
        }

        static int CountLessThanTen(List<int> numbers)
        {
            int count = 0;
            for (int i = 0; i < numbers.Count; i++)
            {
                if (numbers[i] < 10) count++;
            }

            return count;
        }

        static int CountLessThanTenParallel(List<int> numbers)
        {
            int count = 0;

            Parallel.For(0, numbers.Count, new ParallelOptions() { MaxDegreeOfParallelism = -1 }, i =>
             {
                 if (numbers[i] < 10) Interlocked.Increment(ref count);
             });
            return count;
        }

        static int binarySearch_Tasks(int number, List<int> list)
        {
            int indexFound = -1; // This is the variable returned by the function
            Task t1 = new Task(() =>
            {
                // binary searching the first half of the list
                int temp = binarySearch(number, list, 0, list.Count / 2);
                if (temp >= 0) // temp will be >= 0 if a number is found in the first half of the list
                    indexFound = temp;
            });
            Task t2 = new Task(() =>
            {
                // binary searching the second half of the list
                int temp = binarySearch(number, list, (list.Count / 2) + 1, list.Count - 1);
                if (temp >= 0) // temp will be >= 0 if a number is found in the second half of the list
                    indexFound = temp;
            });
            t1.Start();
            t2.Start();

            // Wait for both tasks to finish
            Task.WaitAll(t1, t2);
            return indexFound;
        }

        static int binarySearch_Threads(int number, List<int> list)
        {
            int indexFound = -1; // This is the variable returned by the function
            Thread t1 = new Thread(() =>
            {
                // binary searching the first half of the list
                int temp = binarySearch(number, list, 0, list.Count / 2);
                if (temp >= 0) // temp will be >= 0 if a number is found in the first half of the list
                    indexFound = temp;
            });
            Thread t2 = new Thread(() =>
            {
                // binary searching the second half of the list
                int temp = binarySearch(number, list, (list.Count / 2) + 1, list.Count - 1);
                if (temp >= 0) // temp will be >= 0 if a number is found in the second half of the list
                    indexFound = temp;
            });
            t1.Start();
            t2.Start();

            // Wait for both threads to finish
            t1.Join();
            t2.Join();
            return indexFound;
        }

        static int binarySearch(int number, List<int> list, int startIndex, int endIndex)
        {
            if (endIndex < startIndex)
                return -1; // not found
            int middleIndex = (endIndex + startIndex) / 2;
            if (list[middleIndex] == number)
                return middleIndex;
            else if (list[middleIndex] < number)
                return binarySearch(number, list, middleIndex + 1, endIndex);
            else
                return binarySearch(number, list, startIndex, middleIndex - 1);
        }
    }
}
