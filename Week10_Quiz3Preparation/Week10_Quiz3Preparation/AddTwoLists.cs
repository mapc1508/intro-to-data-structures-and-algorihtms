﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Week10_Quiz3Preparation
{
    public class ListSum
    {
        public static long Compute(List<int> numbers, int firstIndex, int length)
        {
            long product = 0;

            for (int i = firstIndex; i < length; i++)
            {
                product += numbers[i];
            }


            return product;
        }

        public static long ComputeParallel(List<int> numbers)
        {
            long product = 0;
            Parallel.ForEach(numbers, num =>
            {
                product += num;
            });

            return product;
        }

        public static long ComputeUsingTasks(List<int> numbers)
        {
            long result1 = 0, result2 = 0, total = 0;
            //var firstHalf = new List<int>();
            //var secondHalf = new List<int>();

            //for (int i = 0; i < numbers.Count; i++)
            //{
            //    if (numbers[i] % 2 == 0)
            //    {
            //        firstHalf.Add(numbers[i]);
            //    }
            //    else
            //    {
            //        secondHalf.Add(numbers[i]);
            //    }
            //}

            var task1 = new Task(() =>
            {
                result1 = Compute(numbers, 0, numbers.Count / 2);
            });

            var task2 = new Task(() =>
            {
                result2 = Compute(numbers, numbers.Count / 2, numbers.Count);
            });

            task1.Start();
            task2.Start();
            Task.WaitAll(task1, task2);

            return total = result1 + result2;
        }

        public static long ComputeUsingThreads(List<int> numbers)
        {
            long result1 = 0, result2 = 0, total = 0;
            //var firstHalf = new List<int>();
            //var secondHalf = new List<int>();

            //for (int i = 0; i < numbers.Count; i++)
            //{
            //    if (numbers[i] % 2 == 0)
            //    {
            //        firstHalf.Add(numbers[i]);
            //    }
            //    else
            //    {
            //        secondHalf.Add(numbers[i]);
            //    }
            //}


            var thread1 = new Thread(new ThreadStart(() =>
            {
                result1 = Compute(numbers, 0, numbers.Count / 2);
            }));

            var thread2 = new Thread(new ThreadStart(() =>
            {
                result2 = Compute(numbers, numbers.Count / 2, numbers.Count);
            }));

            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();

            return total = result1 + result2;
        }
    }
}
