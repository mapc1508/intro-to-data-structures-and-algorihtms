﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Given number N, make a function that prints Pascal triangle of N rows.

namespace _07_12_2016_Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the number of rows: ");
            int numOfRows = Convert.ToInt32(Console.ReadLine());
            //PascalTriangle(numOfRows);

            for (int i = 0; i < numOfRows; i++)
            {
                for (int y = 0; y < i + 1; y++)
                {
                    Console.Write(PascalTriangleRecursion(i, y) + " ");
                }
                Console.WriteLine();
            }
            //Console.WriteLine(LongestCommonSubstring("mate mateur", "teu teur mateur ma"));
        }

        private static void PascalTriangle(int numOfRows)
        {
            int[] numbers = new int[0];
            var listOfNumbers = new List<int>();

            for (int i = 0; i < numOfRows; i++)
            {
                for (int y = 0; y < i + 1; y++)
                {
                    if (y == 0 || y == numbers.Length)
                    {
                        Console.Write(1 + " ");
                        listOfNumbers.Add(1);
                    }
                    else
                    {
                        int number = numbers[y] + numbers[y - 1];
                        listOfNumbers.Add(number);
                        Console.Write(number + " ");
                    }
                }
                Console.WriteLine();
                numbers = listOfNumbers.ToArray();
                listOfNumbers = new List<int>();
            }
        }

        static int PascalTriangleRecursion(int row, int column)
        {
            if (column == 0 || column == row)
                return 1;
            else
                return PascalTriangleRecursion(row - 1, column - 1) + PascalTriangleRecursion(row - 1, column);
        }

        public static string LongestCommonSubstring(string s1, string s2)
        {
            string longer = s1.Length > s2.Length ? s1 : s2;
            string shorter = s1.Length > s2.Length ? s2 : s1;
            string longestSubstring = "";

            for (int i = 0; i < shorter.Length; i++)
            {
                int length = 0;
                try
                {
                    while (longer.Contains(shorter.Substring(i, ++length)))
                    {
                        if (length > longestSubstring.Length)
                        {
                            longestSubstring = shorter.Substring(i, length);
                        }
                    }
                }
                catch (ArgumentOutOfRangeException) { }
            }

            return longestSubstring;
        }
    }
}

