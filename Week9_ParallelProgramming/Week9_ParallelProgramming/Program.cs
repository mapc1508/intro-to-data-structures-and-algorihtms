﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week9_ParallelProgramming
{
    class Program
    {
        static void Main(string[] args)
        {
            var listOfNumbers = new List<double>();
            var randomGenerator = new Random();

            for (int i = 0; i < 10000000; i++)
            {
                listOfNumbers.Add(randomGenerator.Next(5,15));
            }

            //Console.Write("Enter the double value: ");

            //double number = 0;

            //while ((number = double.Parse(Console.ReadLine())) > 0)
            //{
            //    listOfNumbers.Add(number);
            //    Console.Write("Enter the double value: ");
            //}

            var listOfFactoriels = new List<double>();

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            Parallel.For(0, listOfNumbers.Count, new ParallelOptions() { MaxDegreeOfParallelism = 3 }, i =>
             {
                 listOfFactoriels.Add(Factoriel(listOfNumbers[i]));
             });
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed);

            listOfFactoriels = new List<double>();

            stopwatch.Start();
            foreach (var num in listOfNumbers)
            {
                listOfFactoriels.Add(Factoriel(num));
            }
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed);

            //Console.WriteLine("\nNumbers: ");
            //foreach (var num in listOfNumbers)
            //{
            //    Console.WriteLine(num);
            //}

            //Console.WriteLine("\nNumbers factoriel: ");
            //foreach (var num in listOfFactoriels.OrderBy(n => n))
            //{
            //    Console.WriteLine(num);
            //}
        }

        static double Factoriel(double number)
        {
            if (number <= 1) return 1;
            return Factoriel(number - 1) * number;
        }
    }
}
