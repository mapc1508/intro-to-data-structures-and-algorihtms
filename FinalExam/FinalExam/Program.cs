﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinalExam
{
    class Program
    {
        static void Main(string[] args)
        {
            var outuput = GatMaybeRandomString(new char[] { 'P', 'r', 'o', 'g', 'r', 'a', 'm', 'm', 'i', 'n', 'g' });
            Console.WriteLine(outuput);
            var numbers = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            ReverseIntegers(numbers);
            foreach (var item in numbers)
            {
                Console.WriteLine(item);
            }
            GetPriceInfo(6, 5, 3, 2, 3);
        }

        static string GatMaybeRandomString(char[] characters)
        {
            string output = "";

            Parallel.ForEach(characters, (c) =>
            {
                output += c;
            });

            return output;
        }

        static void ReverseIntegers(List<int> integers)
        {
            //Declaring temp variable outside the for loop to keep the space complexity constant
            var temp = 0;

            /* Iterating through the loop from left to right and vice versa at the same time 
               and switching the values at indexes i and j */
            for (int i = 0, j = integers.Count - 1; i < integers.Count && j > -1; i++, j--)
            {
                //If i >= j there is no need to keep iterating the numbers
                if (i >= j) break;

                //Using the switch two items design pattern
                temp = integers[i];
                integers[i] = integers[j];
                integers[j] = temp;
            }
        }

        static void GetPriceInfo(double roomWidth, double roomLength, double tileWitdh, double tileLength, double price)
        {
            int width = (int)(roomWidth / tileWitdh);
            int length = (int)(roomLength / tileLength);
            int additional = 0;
            int total = 0;

            if (roomWidth % tileWitdh != 0) additional += length;
            if (roomLength % tileLength != 0) additional += width;
            if (roomWidth % tileWitdh != 0 && roomLength % tileLength != 0) additional++;
            total = width * length + additional;
            Console.WriteLine($"Total num of tiles needed {total}.\nNeed to cut: {additional}.\nTotal cost: {price * total}");
        }
    }
}
