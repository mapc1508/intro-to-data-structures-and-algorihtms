﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(StringManipulation.RemoveVowelRecursive("Mirza"));
            Console.WriteLine(StringManipulation.RemoveVowel("Mirza"));
        }
    }

    public static class StringManipulation
    {
        private static IEnumerable<char> GetLongRandomString()
        {
            var randomGenerator = new Random();
            for (int i = 0; i < 1000; i++)
            {
                yield return (char)randomGenerator.Next(48, 122);
            }
        }

        public static string LongRandomString
        {
            get
            {
                return new string(GetLongRandomString().ToArray());
            }
        }
        private static string RemoveVowelRecursive(string input, int index)
        {
            if (index == input.Length) return input;
            switch (char.ToUpper(input[index]))
            {
                case 'A':
                case 'E':
                case 'I':
                case 'O':
                case 'U':
                    input = input.Substring(0, index) + input.Substring(index + 1);
                    break;
                default:
                    index++;
                    break;
            }

            return RemoveVowelRecursive(input, index);
        }

        public static string RemoveVowelRecursive(string input)
        {
            return RemoveVowelRecursive(input, 0);
        }

        public static string RemoveVowel(string input)
        {
            for (int index = 0; index < input.Length; index++)
            {
                switch (char.ToUpper(input[index]))
                {
                    case 'A':
                    case 'E':
                    case 'I':
                    case 'O':
                    case 'U':
                        input = input.Substring(0, index) + input.Substring(index + 1);
                        break;
                    default:
                        break;
                }
            }

            return input;
        }
    }
}
