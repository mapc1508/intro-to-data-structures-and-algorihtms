﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using Exercise1;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var result = StringManipulation.RemoveVowelRecursive("Mirza");
            Assert.AreEqual(result, "Mrz");
        }

        [TestMethod]
        public void TestMethod2()
        {
            var result = StringManipulation.RemoveVowel("Mirza");
            Assert.AreEqual(result, "Mrz");
        }

        [TestMethod]
        public void TestMethod3()
        {
            var longRandomString = StringManipulation.LongRandomString;

            var stopwatch = new Stopwatch();

            stopwatch.Start();
            StringManipulation.RemoveVowel(longRandomString);
            stopwatch.Stop();
            var elapsedNonRecurisve = stopwatch.Elapsed.TotalMilliseconds;

            stopwatch.Restart();
            StringManipulation.RemoveVowelRecursive(longRandomString);
            stopwatch.Stop();
            var elapsedRecursive = stopwatch.Elapsed.TotalMilliseconds;

            Assert.IsTrue(elapsedNonRecurisve < elapsedRecursive);
        }
    }
}
