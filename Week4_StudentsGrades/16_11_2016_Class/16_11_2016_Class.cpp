// 16_11_2016_Class.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "sstream"

using namespace std;

class Student
{
	
	public:
		Student() {};
		int Id;
		string Gender;
		int Year;
		int* Grades;
		int numOfGrades = 0;
};

int main()
{
	int numOfStudents;
	cout << "Enter the number of students: ";
	cin >> numOfStudents;
	cout << endl;

	Student* students;
	students = new Student[numOfStudents];

	for (int i = 0; i < numOfStudents; i++)
	{
		int id, year;
		string gender;
		cout << "Enter Id, gender, year of birth " << endl;
		cin >> students[i].Id >> students[i].Gender >> students[i].Year;
	}

	int id;
	cout << endl << "Enter student id: ";
	cin >> id;

	for (int i = 0; i < numOfStudents; i++)
	{
		Student* student = &students[i];
		if (student->Id == id) {
			int numOfGrades;
			cout << "Enter number of grades:" ;
			cin >> numOfGrades;
			student->numOfGrades = numOfGrades;
			student->Grades = new int[numOfGrades];
			for (int y = 0; y < numOfGrades; y++)
			{
				cout << "Enter the " << y+1 << ". grade: ";
				cin >> student->Grades[y];
			}
		}
	}

	cout << endl;

	for (int i = 0; i < numOfStudents; i++)
	{
		Student student = students[i];
		cout << "ID:" << student.Id << " Gender:" << student.Gender << " Year:" << student.Year << endl;
		for (int y = 0; y < student.numOfGrades; y++)
		{
			cout << "\t" << y + 1 << ". grade:" << student.Grades[y] << endl;
		}
	}
	return 0;
}
