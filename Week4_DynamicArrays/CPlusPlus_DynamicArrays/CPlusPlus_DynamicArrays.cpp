// CPlusPlus_DynamicArrays.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "sstream"
#include "CPlusPlus_DynamicArrays.h"

using namespace std;

int main()
{
	//fixed number of rows
	 /*int* board[4];

	for (int i = 0; i < 4; i++)
	{
		int numOfNames = 0;
		cout << "Enter the num of names you want to insert: ";
		cin >> numOfNames;
		board[i] = new int[numOfNames];
		for (int y = 0; y < numOfNames; y++)
		{
			cout << y + 1 << ". name: ";
			cin >> board[i][y];
		}
	}


	for (int i = 0; i < 4; i++)
	{
		for (int y = 0; y < 3; y++)
		{
			cout << board[i][y] << endl;
		}
	}*/

	int ** board;
	int numOfRows, numOfColumns;
	cout << "Enter the number of rows and columns: ";
	cin >> numOfRows >> numOfColumns;
	board = new int*[numOfRows];
	for (int i = 0; i < numOfRows; i++)
	{
		board[i] = new int[numOfColumns];
	}

	fill(board, numOfRows, numOfColumns);
	cout << endl;
	print(board, numOfRows, numOfColumns);
	return 0;
}

void fill(int** board, int numOfRows, int numOfColumns) {
	for (int i = 0; i < numOfRows; i++)
	{
		cout << "Enter elements of the row number " << i + 1 << endl;
		for (int y = 0; y < numOfColumns; y++)
		{
			cin >> board[i][y];
		}
	}
}

void print(int** board, int numOfRows, int numOfColumns) {
	for (int i = 0; i < numOfRows; i++)
	{
		for (int y = 0; y < numOfColumns; y++)
		{
			cout << board[i][y] << " ";
		}
		cout << endl;
	}
}

