// Example_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "sstream"
#include "string"

/*
Write a program that prompts the user to input three numbers. The
program should then output the numbers in ascending order.
*/

using namespace std;

int main()
{
	int arrayOfNumbers[10];
	int length = sizeof(arrayOfNumbers)/sizeof(arrayOfNumbers[0]);

	for (int i = 0; i < length; i++)
	{
		cout << i + 1 << ". number: ";
		cin >> arrayOfNumbers[i];
		cout << endl;
	}

	bool hasBeenChanged = false;
	
	for (int i = 1; i <= length-1; i++)
	{	
		int x = arrayOfNumbers[i - 1];

		if (arrayOfNumbers[i] < x) {
			arrayOfNumbers[i - 1] = arrayOfNumbers[i];
			arrayOfNumbers[i] = x;
			hasBeenChanged = true;
		}

		if (i == length - 1 && hasBeenChanged) {
			i = 0;
			hasBeenChanged = false;
		}
	}

	for each (int number in arrayOfNumbers)
	{
		cout << number << endl;
	}
	return 0;
}

