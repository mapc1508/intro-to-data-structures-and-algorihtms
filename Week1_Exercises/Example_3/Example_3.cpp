// Example_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

/*
� For input positive integer draw a structure like below:
(for input = 5) (for input = 4) (for input = 1)
***
****
*****
***
*
For invalid input end program with a proper message.
*/

int main()
{
	char symbol = '*';
	int num_of_lines = 0;
	cout << "Enter the number of iterations: ";
	cin >> num_of_lines;
	
	int z = num_of_lines;
	int num_of_symbols = 1;
	
	for (int i = 1; i <= num_of_lines; i++)
	{
		z--;
		for (int y = 1; y <= num_of_symbols; y++)
		{
			cout << symbol;
		}
		cout << endl;

		if (i < z) {
			num_of_symbols++;
		}
		else if(i > z) {
			num_of_symbols--;
		}
	}
	return 0;
}

