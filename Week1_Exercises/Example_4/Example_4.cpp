// Example_4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

/*
� For input radius of a circle (of type float), calculate and output its
perimeter and surface area. For invalid input end program with a
proper message.
*/

int main()
{
	float radius;
	cout << "Enter the radius: ";
	const float pi = 3.14;

	if ((cin >> radius)) {
		float perimeter = 2 * radius* pi;
		float surface_area = pi * radius * radius;
		cout << "Perimeter: " << perimeter << ". Surface area: " << surface_area << endl;
	}
	else {
		cout << "You have not entered radius properly.";
	}
	return 0;
}

