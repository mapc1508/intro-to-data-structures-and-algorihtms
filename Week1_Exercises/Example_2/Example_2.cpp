// Example_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

/*
� For input 2 integers and 1 character, draw a rectangle like below:
(for input 10, 3, �X�)
XXXXXXXXXX
X X
XXXXXXXXXX
If input is invalid, make user input everything again.
*/

using namespace std;

int main()
{
	char symbol = ' ';
	int height = 0;
	int width = 0;
	bool inputCorrect = true;
	do
	{
		inputCorrect = true;
		cout << "Enter the symbol you want to use: ";
		cin >> symbol;
		cout << endl;
		cout << "Enter the height: ";
		cin >> height;
		cout << endl;
		cout << "Enter the length: ";
		cin >> width;
		cout << endl;

		if (!cin) {
			cout << "Input is wrong. Please do it again." << endl;
			inputCorrect = false;
		}
		cin.clear();
	} while (!inputCorrect);
	
	for (int i = 1; i <= height; i++)
	{
		if (i == 1 || i == height) {
			for (int y = 1; y <= width; y++)
				cout << symbol;
		}
		else {
			for (int y = 1; y <= width; y++)
			{
				if (y == 1 || y == width)
					cout << symbol;
				else cout << ' ';
			}
		}
		cout << endl;
	}
	return 0;
}

