// 31_10_16_Class.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;


int main()
{
	char symbol = '*';
	int num_of_iterations = 0;
	cout << "Enter the number of iterations: ";
	cin >> num_of_iterations;
	for (int i = 0; i < num_of_iterations; i++)
	{
		for (int y = 0; y <= i; y++)
		{	
			cout << symbol;
		}
		cout << endl;
	}
    return 0;
}

