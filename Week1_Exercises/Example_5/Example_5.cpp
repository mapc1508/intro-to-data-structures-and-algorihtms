// Example_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

/*
� For an input positive integer N, list all prime numbers from 1 to N,
inclusive. If input is invalid, make user input everything again.
For example:
- for input 10, the output should be: 2, 3, 5, 7
*/

int main()
{
	int n;
	cout << "Enter an integer number N: ";
	cin >> n;

	if (n == 2) cout << 2;
	else if (n > 2) {
		cout << 2 << endl << 3;
		for (int i = 2; i < n; i++)
		{
			int last = i / 2;
			for (int y = 2; y <= last; y++)
			{
				if (i % y == 0) break;
				if (y == last) cout << endl << i;
			}
		}
	}
	cout << endl;
	return 0;
}