﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initializing the list of unsorted double numbers
            var numbers = new List<double>() { 3.5, 2.4, 6.1, 4.9, 9.1, 5.2, 1.5, 10.2, 8.4, 7.3 };

            //Sorting the list using the quicksort
            Sort.QuickSort(numbers);

            //Printing the sorted list
            foreach (var num in numbers)
            {
                Console.WriteLine(num);
            }
        }
    }

    public static class Sort
    {
        public static void QuickSort(List<double> numbers)
        {
            /* Method that calls the private QuickSort() overloaded version 
               and provides lowIndex and pivot values for the first recursive call */
            QuickSort(numbers, 0, numbers.Count - 1);
        }

        private static void QuickSort(List<double> numbers, int lowIndex, int pivot)
        {
            //pivot - index of a number used for partitioning (in this case the rightmost number)
            //lowIndex - starting index of numbers lower or equal to the pivot
            //highIndex - starting index of numbers higher than the pivot
            if (lowIndex < pivot)
            {
                int highIndex = lowIndex;
                double temp = 0;

                //Parallelizing for loop would not be safe beacause of the depenendencies
                for (int i = lowIndex; i < pivot; i++)
                {
                    /* If current number in iteration is lower than pivot it is placed 
                       inside lowIndex..highIndex-1 subarray */
                    if (numbers[i] <= numbers[pivot])
                    {
                        temp = numbers[i];
                        numbers[i] = numbers[highIndex];
                        numbers[highIndex] = temp;
                        highIndex++;
                    }
                }

                /* Placing pivot between subarray of numbers lower than pivot and 
                   subbarray of numbers that are higher than pivot */
                temp = numbers[pivot];
                numbers[pivot] = numbers[highIndex];
                numbers[highIndex] = temp;

                //Recursively sorting the numbers lower than pivot and the numbers higher than pivot
                //Two methods are executed in parallel
                Parallel.Invoke(
                    () => QuickSort(numbers, lowIndex, highIndex - 1),
                    () => QuickSort(numbers, highIndex + 1, pivot));
            }
        }
    }
}
