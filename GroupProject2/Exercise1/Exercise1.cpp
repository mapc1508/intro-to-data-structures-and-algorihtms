#include "stdafx.h"
#include <iostream>

using namespace std;

void bubble(int numbers[], size_t size);

int main(void)
{
	int n;
	int* numbers;

	//Entering the size and elements of an array
	cout << "Enter the size of the array: ";
	cin >> n;

	numbers = new int[n];

	cout << "Enter the array elements: ";

	for (int i = 0; i < n; ++i)
	{
		cin >> numbers[i];
	}

	//Calling bubble function
	bubble(numbers, n);


	//Showing the "numbers" array after the Bubble sort
	cout << "Array after bubble sort:";

	for (int i = 0; i < n; ++i)
	{
		cout << " " << numbers[i];
	}

	cout << endl;
}

void bubble(int numbers[], size_t size)
{
	int temp;
	bool again = true;
	int iter = size - 1;

	//do .. while loop for the Bubble sort
	do {
		again = false;
		for (int i = 0; i < iter; ++i)
		{
			if (numbers[i + 1] < numbers[i])
			{
				again = true;
				temp = numbers[i];
				numbers[i] = numbers[i + 1];
				numbers[i + 1] = temp;
			}
		}

		--iter;
	} while (again); // repeat the loop until it comes to the last element in "numbers" array
}


