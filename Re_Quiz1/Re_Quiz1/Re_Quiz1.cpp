#include "stdafx.h"
#include "iostream"
#include "sstream";
using namespace std;

/*
A: 65
Z: 90
a: 97
z: 122

For an input sentence and positive number N, output the same sentence with every N-th word written in uppercase. 
Non-letter symbols must be left unchanged. If an input is invalid, user should be forced to redo it. 
You may assume that a word is any set of characters without whitespace.
*/

int main()
{
	int n = 2;
	bool validInput = false;
	string sentence = "";
	cout << "Enter your sentence: ";
	getline(cin, sentence);
	
	do
	{
		validInput = true;
		cout << "Enter n: ";
		cin >> n;

		if (n < 0) {
			cout << "Your input is not valid, n must be a positive number" << endl;
			validInput = false;
		}
	} while (validInput == false);

	int startWordIndex = 0;
	char currentCharacter;
	int wordCount = 1;

	for (int i = 0; i < sentence.length(); i++)
	{
		if (wordCount % n == 0) {
			if ((int)sentence[i] >= 97 && (int)sentence[i] <= 122) {
				cout << (char)(sentence[i] - 32);
			}
			else cout << sentence[i];
		}
		else cout << sentence[i];
		if (sentence[i] == ' ') wordCount++;
	}

	cout << endl;
	
    return 0;
}

