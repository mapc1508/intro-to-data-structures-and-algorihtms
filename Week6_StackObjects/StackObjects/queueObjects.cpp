﻿#include "stdafx.h"
#include "queueObjects.h"
#include "iostream"
#include "sstream"
using namespace std;

Queue::Queue()
{
}

void Queue::Enqueue(Student value)
{
	Node* temp = new Node();
		temp->item = value;
	
		if (start == NULL) {
			start = temp;
			end = temp;
		}
		else {
			end->next = temp;
			end = temp;
		}
		length++;
}

Student Queue::Dequeue()
{
	length--;
	if (length < 0) throw "Queue is empty";
	Node* temp = start;
	Student returnObject = start->item;
	start = temp->next;
	delete temp;
	return returnObject;
}

void Queue::Print()
{
	while (GetLength() > 0)
	{
		cout << Dequeue().GetName() << endl;
	}
}

void Queue::Delete(Student value)
{
	Node* temp = start;

	while (temp->next != NULL && temp->next->item.GetName() != value.GetName()) {
		temp = temp->next;
	}

	if (temp->next != NULL) {
		Node* target = temp->next;
		temp->next = target->next;
		delete target;
		length--;
	}
}

void Queue::Empty()
{
	Node* temp;
	while ((temp = start) != NULL) {
		start = start->next;
		delete temp;
	}
	length = 0;
}

Student Queue::Peek()
{
	if (start)
		return start->item;
	else
		return "Stack is empty\n";
}

int Queue::GetLength()
{
	return length;
}

