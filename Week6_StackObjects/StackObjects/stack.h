﻿#pragma once
#include "iostream"

class Student
{
public:
	Student(std::string studentName);
	std::string GetName();

private:
	std::string name;
};

class Node
{
public:
	Node();
	Student item = Student("");
	Node* next;

private:

};

class Stack
{
public:
	Stack();
	void Push(Student value);
	Student Pop();
	void Print();
	void Delete(Student value);
	void Empty();
	Student Peek();
	int GetLength();

private:
	Node* start;
	Node* end;
	int length;
};