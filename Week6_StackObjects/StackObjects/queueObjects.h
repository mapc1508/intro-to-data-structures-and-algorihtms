﻿#pragma once
#include "stack.h"
#include "iostream"

class Queue
{
public:
	Queue();
	void Enqueue(Student value);
	Student Dequeue();
	void Print();
	void Delete(Student value);
	void Empty();
	Student Peek();
	int GetLength();

private:
	Node* start;
	Node* end;
	int length;
};