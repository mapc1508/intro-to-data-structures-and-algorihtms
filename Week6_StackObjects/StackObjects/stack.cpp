﻿#include "stdafx.h"
#include "stack.h"
#include "iostream"
#include "sstream"
using namespace std;

Student::Student(string studentName)
{
	name = studentName;
}

string Student::GetName()
{
	return name;
}

Node::Node() {}


Stack::Stack() {
	length = 0;
	start = NULL;
}

void Stack::Push(Student value)
{
	Node* temp = new Node();
	temp->item = value;

	if (start == NULL) {
		start = temp;
	}
	else {
		temp->next = start;
		start = temp;
	}

	length++;
}

Student Stack::Pop()
{
	length--;
	if (length < 0) throw "Stack is empty";
	Node* temp = start;
	Student returnObject = start->item;
	start = temp->next;
	delete temp;
	return returnObject;
}

void Stack::Print()
{
	while (GetLength() > 0)
	{
		cout << Pop().GetName() << endl;
	}
}

void Stack::Delete(Student value)
{
	Node* temp = start;

	if (start->item.GetName() == value.GetName()) {
		start = start->next;
		delete temp;
		length--;
	}
	else {
		while (temp->next != NULL && temp->next->item.GetName() != value.GetName()) {
			temp = temp->next;
		}

		if (temp->next != NULL) {
			Node* target = temp->next;
			temp->next = target->next;
			delete target;
			length--;
		}
	}
}

void Stack::Empty()
{
	Node* temp;
	while ((temp = start) != NULL) {
		start = start->next;
		delete temp;
	}
	length = 0;
}

Student Stack::Peek()
{
	if (start)
		return start->item;
	else
		return "Stack is empty\n";
}

int Stack::GetLength()
{
	return length;
}


