// StackObjects.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stack.h"
#include "queueObjects.h"
#include "iostream"
#include "sstream"

using namespace std;

int main()
{
	cout << "Stack:" << endl << endl;
	
	Stack stack;

	stack.Push(Student("Amina"));
	stack.Push(Student("Anela"));
	stack.Push(Student("Dima"));
	stack.Push(Student("Nera"));
	stack.Push(Student("Elma"));
	stack.Delete(Student("Elma"));
	stack.Print();

	cout << endl << "Queue:" << endl << endl;

	Queue queue;

	queue.Enqueue(Student("Amina"));
	queue.Enqueue(Student("Anela"));
	queue.Enqueue(Student("Dima"));
	queue.Enqueue(Student("Nera"));
	queue.Enqueue(Student("Elma"));

	queue.Print();

	//cout << endl << stack.Peek().GetName() << endl;
    
	return 0;
}

