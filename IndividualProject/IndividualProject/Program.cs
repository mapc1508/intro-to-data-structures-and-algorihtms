﻿using System;
using System.Linq;
using System.Threading;

namespace IndividualProject
{
    class Program
    {
        static void Main(string[] args)
        {
            var digitalClock = new DigitalClock();

            //Setting a custom symbol
            //digitalClock.Symbol = '@';
            digitalClock.FontColor = ConsoleColor.Yellow;
            digitalClock.Show();
        }
    }

    public class DigitalClock
    {
        public DigitalClock()
        {
            cursorOffsetLeft = 0;
            showColon = true;
            Console.CursorVisible = false;
            Console.Title = "Digital Clock";
        }

        private int cursorOffsetLeft;
        private bool showColon;

        //The default symbol is ▓
        public char Symbol { get; set; } = '▓';

        //Font color used to print symbols
        public ConsoleColor FontColor
        {
            get { return FontColor; }
            set { Console.ForegroundColor = value; }
        }

        private void Convert()
        {
            //Initializing the current time in military time format
            var timeNow = DateTime.Now.ToString("HH:mm:ss");

            //Used for setting the cursor's starting point for each individual character of the clock
            cursorOffsetLeft = 0;

            //Used to get the colon character to blink
            showColon = !showColon;

            //Converting each character of the time string to appropriate clock element
            foreach (var character in timeNow)
            {
                switch (character)
                {
                    case '0':
                        PrintClockElement(5, "1 2", "2 2", "3 2");
                        break;
                    case '1':
                        PrintClockElement(5, "0 012", "1 01", "2 012", "3 012", "4 012");
                        break;
                    case '2':
                        PrintClockElement(5, "1 012", "3 234");
                        break;
                    case '3':
                        PrintClockElement(5, "1 012", "3 012");
                        break;
                    case '4':
                        PrintClockElement(5, "0 234", "1 234", "3 012", "4 012");
                        break;
                    case '5':
                        PrintClockElement(5, "1 234", "3 012");
                        break;
                    case '6':
                        PrintClockElement(5, "1 234", "3 2");
                        break;
                    case '7':
                        PrintClockElement(5, "1 012", "2 012", "3 012", "4 012");
                        break;
                    case '8':
                        PrintClockElement(5, "1 2", "3 2");
                        break;
                    case '9':
                        PrintClockElement(5, "1 2", "3 012");
                        break;
                    case ':':
                        if (showColon) PrintClockElement(2, "0 01", "2 01", "4 01");
                        else PrintClockElement(2, "0 01", "1 01", "2 01", "3 01", "4 01");
                        break;
                }
            }
        }

        ///<summary>
        ///<para>Outputs the clock element based on its width and coordinates of the whitespaces</para>
        ///<para>Hint: Coordinate "0 13" means that the whitespaces appear in the second and fourth column of the first row (zero-indexed)</para>
        ///</summary>
        private void PrintClockElement(int width, params string[] coordinates)
        {
            string rowWithWhitespaces = string.Empty;
            Console.CursorLeft = cursorOffsetLeft;

            for (int row = 0; row < 5; row++)
            {
                Console.CursorTop = row;

                //If the current row contains whitespaces return its coordinate string 
                rowWithWhitespaces = coordinates.FirstOrDefault(c => c[0].ToString() == System.Convert.ToString(row));
                for (int col = 0; col < width; col++)
                {
                    //If inside a row with whitespaces check which columns are whitespace characters
                    if (rowWithWhitespaces != null)
                    {
                        if (rowWithWhitespaces.Substring(2).Contains(col.ToString()))
                        {
                            Console.Write(' ');
                            continue;
                        }
                    }
                    Console.Write(Symbol);
                }
                Console.CursorLeft = cursorOffsetLeft;
            }

            //Move cursor to the right so that the elements can be printed next to each other
            cursorOffsetLeft += (width + 2);
        }

        ///<summary>
        ///<para>Outputs the digital clock.</para>
        ///</summary>
        public void Show()
        {
            //Timer calls the Convert() method repeatedly after the interaval of 1s has passed
            var timer = new Timer(c => Convert(), null, TimeSpan.Zero, TimeSpan.FromSeconds(1));

            //The program stops when user presses any key
            Console.ReadKey();
        }
    }
}
